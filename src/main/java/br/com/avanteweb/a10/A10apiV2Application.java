package br.com.avanteweb.a10;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class A10apiV2Application implements CommandLineRunner{


	public static void main(String[] args) {
		SpringApplication.run(A10apiV2Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}
}
