package br.com.avanteweb.a10.controllers;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataBase {
	private static final Logger log = LoggerFactory.getLogger(DataBase.class);

	private Connection conexao;
	public Statement comando;
	private ResultSet rs;

	private String descricaoDriver;

	private String endereco;
	private String usuario;
	private String senha;

	public DataBase(){
        descricaoDriver = "oracle.jdbc.OracleDriver";
        usuario = "A10_PRINCIPAL";
        senha = "A10j2x";
    }

	public void defineDriver(String descricao) {
		descricaoDriver = descricao;
	}

	public void defineEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void defineUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void defineSenha(String senha) {
		this.senha = senha;
	}

	private void carregaDriver() {
		try {
			Class.forName(descricaoDriver);
		} catch (ClassNotFoundException erro) {
			log.info("FALHA AO CARREGAR O DRIVER - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}
	}

	public boolean abreConexao() {
		try {
			carregaDriver();
			conexao = DriverManager.getConnection(endereco, usuario, senha);

			comando = conexao.createStatement();
			return true;
		} catch (SQLException erro) {
			log.info("FALHA AO ABRIR CONEXAO - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
			return false;
		}
	}

	public void fechaConexao() {
		try {
			comando.close();
			conexao.close();
		} catch (SQLException erro) {
			log.info("FALHA AO FECHAR CONEXAO - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}
	}

	public Connection retornaConexao() {
		return conexao;
	}

	public boolean executaComando(String comandoSQL) {
		try {
			comando.executeUpdate(comandoSQL);
			return true;
		} catch (SQLException erro) {
			log.info("FALHA EXECUTAR COMANDO - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
			return false;
		}
	}

	public void executaConsulta(String comandoSQL) {
		try {
			rs = comando.executeQuery(comandoSQL);
		} catch (SQLException erro) {
			log.info("FALHA EXECUTAR CONSULTA - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}
	}

	public boolean proximoRegistro() {
		boolean status = false;
		try {
			status = rs.next();
		} catch (SQLException erro) {
			log.info("FALHA PROXIMO REGISTRO - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}
		return status;
	}

	public int retornaInt(String atributo) {
		int valor = 0;
		try {
			valor = rs.getInt(atributo);
		} catch (SQLException erro) {
			log.info("FALHA RETORNA INT - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}
		return valor;
	}

	public float retornaFloat(String atributo) {
		float valor = 0.0f;
		try {
			valor = rs.getFloat(atributo);
		} catch (SQLException erro) {
			log.info("FALHA RETORNA FLOAT - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}
		return valor;
	}

	public String retornaString(String atributo) {
		String valor = "";
		try {
			valor = rs.getString(atributo);
		} catch (SQLException erro) {
			log.info("FALHA RETORNA STRING - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}
		return valor;
	}

	public Date retornaData(String atributo) {
		Date valor = null;
		try {
			valor = rs.getDate(atributo);
		} catch (SQLException erro) {
			log.info("FALHA RETORNA DATA - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}

		return valor;
	}

	public String retornaDataParaString(String atributo) {
		String valor = "";
		try {
			valor = rs.getString(atributo);
		} catch (SQLException erro) {
			log.info("FALHA DATA PARA STRING - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}

		String dia = "" + valor.charAt(8) + valor.charAt(9);
		String mes = "" + valor.charAt(5) + valor.charAt(6);
		String ano = "" + valor.charAt(0) + valor.charAt(1) + valor.charAt(2) + valor.charAt(3);
		valor = dia + "/" + mes + "/" + ano;

		return valor;
	}

	public double retornaDouble(String atributo) {
		double valor = 0.0;
		try {
			valor = rs.getDouble(atributo);
		} catch (SQLException erro) {
			log.info("FALHA RETORNA DOUBLE - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
		}
		return valor;
	}

	public Integer retornaSchema() {
		try {
			CallableStatement call = conexao.prepareCall("{call PROC_RETORNO_SCHEMA(?)}");
			call.registerOutParameter(1, java.sql.Types.NUMERIC);

			call.execute();

			Integer schema = call.getInt(1);

			return schema;
		} catch (SQLException erro) {
			log.info("FALHA RETORNA SCHEMA - " + erro.toString());
			erro.printStackTrace();
			log.error(erro.getStackTrace().toString());
			return null;
		}
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
