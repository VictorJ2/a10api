package br.com.avanteweb.a10.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.avanteweb.a10.entities.Cliente;
import br.com.avanteweb.a10.entities.Contrato;
import br.com.avanteweb.a10.entities.Usuario;
import br.com.avanteweb.a10.entities.UsuarioCliente;
import br.com.avanteweb.a10.exception.services.ErroService;
import br.com.avanteweb.a10.models.gfsis.ClienteModelGfsis;
import br.com.avanteweb.a10.models.gfsis.ContratoModelGfsis;
import br.com.avanteweb.a10.models.gfsis.GrupoModelGfsis;
import br.com.avanteweb.a10.models.gfsis.ItensModelGfsis;
import br.com.avanteweb.a10.models.utils.VerificacaoCampos;
import br.com.avanteweb.a10.retorna.endpoints.RetornaClienteEndPoint;
import br.com.avanteweb.a10.retorna.endpoints.RetornaContratoEndPoint;
import br.com.avanteweb.a10.retorna.endpoints.RetornaItensEndPoint;
import br.com.avanteweb.a10.retorna.endpoints.RetornaUsuarioClienteEndPoint;
import br.com.avanteweb.a10.retorna.endpoints.RetornaUsuarioEndPoint;
import br.com.avanteweb.a10.services.ClienteService;
import br.com.avanteweb.a10.services.ContratoService;
import br.com.avanteweb.a10.services.UsuarioClienteService;
import br.com.avanteweb.a10.services.UsuarioService;
import br.com.avanteweb.a10.utils.PasswordUtil;
import br.com.avanteweb.a10.utils.RemoveAcentos;

@RestController
@CrossOrigin(origins = "*")
public class NewContratoController {
	private static final Logger log = LoggerFactory.getLogger(NewContratoController.class);

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioClienteService usuarioClienteService;

	@Autowired
	private ContratoService contratoService;

	public NewContratoController() {
	}

	@PostMapping("/api/contrato") // Interrogação é para retorno de qualquer valor
	public ContratoModelGfsis cadastrar(@RequestBody ContratoModelGfsis allContrato,
			@RequestHeader(name = "tokens", required = false) String token) throws ErroService {
		// ResponseEntity recebe os dados da requisição html

		log.info("\n");
		log.info("#########################################################################################");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### -------------------- CONSULTAR SITUACAO CONTRATO GFSIS -------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("#########################################################################################");
		log.info("\nAtualizando Cadastrando Gfsis: {}", allContrato.getId());

		Contrato newContrato = new RetornaContratoEndPoint().conversaoParaContrato(allContrato);
		log.info("Buscado dados do contrato: " + newContrato.toString());

		log.info("Verificando campos obrigatórios no contrato");
		if (!new VerificacaoCampos().verificaCamposObrigatorios(allContrato))
			return null;

		Contrato oldContrato = contratoService.findContratoByIdGfsis(newContrato.getIdGfsis());
		Long idContratoExistente = null;
		if (oldContrato != null) {
			if(oldContrato.getIdGfsis().longValue() == 216) {
				log.info("Empresa para da suporte do A10");
				return null;
			}
			idContratoExistente = oldContrato.getIdContrato();
			log.info("ID do contrato existente: " + idContratoExistente);
		}

		log.info("Verificando Status do contrato");
		log.info("Status vindo: " + allContrato.getSituacao());
		if (allContrato.getSituacao().contains("Aguardando aceite")) {
			return null;
		}

		log.info("Lendo Cliente da Requisição!");
		ClienteModelGfsis clienteModelGfsis = allContrato.getCliente();
		Cliente cliente = new RetornaClienteEndPoint().conversaoParaCliente(clienteModelGfsis);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
			cliente.setDataCadastro(sdf.parse(allContrato.getDataInicio()));
		} catch (Exception e) {
			cliente.setDataCadastro(new Date());
		}

		log.info("Buscando dados do cliente: " + cliente.toString());
		Cliente retornaCliente = clienteService.findClienteByIdGfsis(cliente.getIdGfsis());

		// Caso em que pode existir contrato antigo do cliente e chega schema antigo
		// finalizado
		// Tratando para nao modificar o cliente atual
		if (retornaCliente != null) {
			List<Contrato> contratosExistentes = contratoService.findContratoByIdCliente(retornaCliente.getIdCliente());
			Long idNaoExisteFinalizado = null;
			if (contratosExistentes != null) {
				for (Contrato aux : contratosExistentes) {
					if (!aux.getSituacao().contains("Finalizado")) {
						idNaoExisteFinalizado = aux.getIdGfsis();
						break;
					}
				}

				if (idNaoExisteFinalizado != null
						&& idNaoExisteFinalizado.longValue() != allContrato.getId().longValue()
						&& allContrato.getSituacao().contains("Finalizado")) {
					log.info("Ja existe contrato diferente de finalizado e chegou finalizado, processo termina aqui!");
					return null;

				}
			}
		}

		if (!allContrato.getSituacao().contains("Finalizado")) {

			log.info("Lendo Plano da Requisição!");
			List<ItensModelGfsis> itens = allContrato.getItens();
			List<String> listaIcms = new RetornaItensEndPoint().retornaPlanoIcms(itens);
			List<String> listaIss = new RetornaItensEndPoint().retornaPlanoIss(itens);
			if ((listaIcms == null || listaIcms.isEmpty()) && (listaIss == null || listaIss.isEmpty())) {
				log.info("Erro ao obter plano");
				return null;
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			String planoIcms = null;
			Date dataIcmsInicio = null;
			Date dataIcmsFim = null;
			if (listaIcms.size() > 0) {
				try {
					planoIcms = listaIcms.get(0);
					dataIcmsInicio = sdf.parse(listaIcms.get(1));
					if (listaIcms.get(2) != null) {
						dataIcmsFim = sdf.parse(listaIcms.get(2));
					}
				} catch (ParseException e) {
				}
			}

			String planoIss = null;
			Date dataIssInicio = null;
			Date dataIssFim = null;
			if (listaIss.size() > 0) {
				try {
					planoIss = listaIss.get(0);
					dataIssInicio = sdf.parse(listaIss.get(1));
					if (listaIss.get(2) != null) {
						dataIssFim = sdf.parse(listaIss.get(2));
					}
				} catch (ParseException e) {
				}
			}

			log.info("Extraindo grupo do contrato: " + clienteModelGfsis.getGrupo());
			GrupoModelGfsis grupo = clienteModelGfsis.getGrupo();

			if (clienteModelGfsis.getId() != null) {
				Usuario usuario;
				if (retornaCliente == null) {
					cliente = this.cadastraCliente(cliente).getBody();
					cliente.setPlanoIcms(planoIcms);
					cliente.setPlanoDataInicioIcms(dataIcmsInicio);
					cliente.setPlanoDataFimIcms(dataIcmsFim);
					cliente.setPlanoIss(planoIss);
					cliente.setPlanoDataInicioIss(dataIssInicio);
					cliente.setPlanoDataFimIss(dataIssFim);
					usuario = this.cadastraUsuario(cliente, cliente.getSchema(), grupo).getBody();
					if (usuario != null) {
						clienteService.persistir(cliente);
						UsuarioCliente userCli = this
								.cadastraUsuarioCliente(usuario.getIdUsuario(), cliente.getIdCliente()).getBody();
						usuarioClienteService.persistir(userCli);

						if (grupo != null) {
							log.info("Email vinculado a mais empresas: " + grupo.toString());
							Usuario grupoUsuario = usuarioService.findUsuarioByIdGfsis(grupo.getId());
							UsuarioCliente userCliGrupo = this
									.cadastraUsuarioCliente(grupoUsuario.getIdUsuario(), cliente.getIdCliente())
									.getBody();
							log.info("Gravando UsuárioCliente, ID Usuário: " + userCliGrupo.getIdUsuario()
									+ ", ID Cliente: " + userCliGrupo.getIdClienteUsuario());
							usuarioClienteService.persistir(userCliGrupo);
						}

					} else {
						log.warn("Erro ao cadastrar usuário!");
						return null;
					}
				} else {
					Usuario oldUsuario = usuarioService.findUsuarioByIdGfsis(retornaCliente.getIdGfsis());
					usuario = this.atualizaUsuario(cliente, oldUsuario, retornaCliente.getSchema()).getBody();

					if (usuario != null) {
						cliente.setPlanoIcms(planoIcms);
						cliente.setPlanoDataInicioIcms(dataIcmsInicio);
						cliente.setPlanoDataFimIcms(dataIcmsFim);
						cliente.setPlanoIss(planoIss);
						cliente.setPlanoDataInicioIss(dataIssInicio);
						cliente.setPlanoDataFimIss(dataIssFim);
						cliente = this.atualizaCliente(cliente, retornaCliente).getBody();
						cliente = new RetornaClienteEndPoint().atualizaSituacaoCliente(newContrato, cliente);
						cliente.setDataCadastro(retornaCliente.getDataCadastro());
						clienteService.persistir(cliente);
						UsuarioCliente userCli = this.atualizaUsuarioCliente(cliente.getIdCliente(),
								usuario.getIdUsuario(), oldUsuario.getIdUsuario()).getBody();
						usuarioClienteService.persistir(userCli);

//						if(grupo != null) {
//							log.info("Email vinculado a mais empresas: "+grupo.toString());
//							Usuario grupoUsuario = usuarioService.findUsuarioByIdGfsis(grupo.getId());
//							UsuarioCliente userCliGrupo = this
//									.cadastraUsuarioCliente(grupoUsuario.getIdUsuario(), cliente.getIdCliente()).getBody();
//							log.info("Gravando UsuárioCliente, ID Usuário: "+userCliGrupo.getIdUsuario()
//									+", ID Cliente: "+userCliGrupo.getIdClienteUsuario());
//							usuarioClienteService.persistir(userCliGrupo);
//						}

					} else {
						log.warn("Erro ao atualizar Usuario!");
						return null;
					}
				}
			} else {
				log.warn("Cliente sem idGFSIS!!!!!!");
				return null;
			}
		} else {
			if (retornaCliente != null) {
				retornaCliente.setIdGfsis(null);
				retornaCliente.setInativa(1);
				retornaCliente.setExcluida(0);
				clienteService.persistir(retornaCliente);

				List<UsuarioCliente> listaASerDeletada = usuarioClienteService
						.findByIdCliente(retornaCliente.getIdCliente());
				if (listaASerDeletada != null)
					for (UsuarioCliente userCli : listaASerDeletada) {
						usuarioService.deletarUsuarioById(userCli.getIdUsuario());
					}
				log.info("Lista de Usuários deletada");
				log.info("#########################################################################################\n");
			}
		}

		if (retornaCliente == null && allContrato.getSituacao().contains("Finalizado")) {
			log.info("Tentativa de cadastro de cliente com situação Finalizado!!!!!!!!!!");
			return allContrato;
		}
		if (retornaCliente != null) {
			newContrato = this.cadastraContrato(allContrato, retornaCliente.getIdCliente()).getBody();
		} else {
			newContrato = this.cadastraContrato(allContrato, cliente.getIdCliente()).getBody();
		}
		newContrato.setIdContrato(idContratoExistente);
		contratoService.persistir(newContrato);
		log.info("Executado toda operação do Contrato!!!!!!!!!!!!!");
		return allContrato;
	}

	public ResponseEntity<Contrato> cadastraContrato(ContratoModelGfsis contratoModel, long id) throws ErroService {
		Contrato contrato = new RetornaContratoEndPoint().conversaoParaContrato(contratoModel);
		contrato.setIdCliente(id);
		return ResponseEntity.ok().body(contrato);
	}

	public ResponseEntity<Contrato> atualizaContrato(Contrato newContrato, Contrato oldContrato) throws ErroService {
		Contrato contrato = new RetornaContratoEndPoint().atualizaContrato(newContrato, oldContrato);
		return ResponseEntity.ok().body(contrato);
	}

	public ResponseEntity<Cliente> cadastraCliente(Cliente cliente) throws ErroService {
		return ResponseEntity.ok().body(cliente);
	}

	public ResponseEntity<Cliente> atualizaCliente(Cliente newCliente, Cliente oldCliente) throws ErroService {
		Cliente cliente = new RetornaClienteEndPoint().atualizaCliente(newCliente, oldCliente);
		return ResponseEntity.ok().body(cliente);
	}

	public ResponseEntity<Usuario> cadastraUsuario(Cliente cliente, String schema, GrupoModelGfsis grupo)
			throws ErroService {
		Usuario usuario = new RetornaUsuarioEndPoint().conversorParaUsuario(cliente);
		if (!usuarioService.findByEmail(usuario.getEmail())) {
			usuario = usuarioService.persistir(usuario);
			usuario.setSenha(PasswordUtil.gerarBCrypt(usuario.getIdUsuario(), "123"));
			usuarioService.persistir(usuario);
		} else {
			log.warn("Existe usuario com mesmo email no banco");
			return ResponseEntity.ok().body(null);
		}

		log.info("Usuario de id: " + usuario.getIdUsuario() + " e id GFSIS: " + usuario.getIdGfsis() + " cadastrado!");
		SchemaController schemaController = new SchemaController();

		if (schema == null) {
			log.info("CRIANDO NOVO SCHEMA...");
			schema = schemaController.criaSchema();
		}
		log.info("SCHEMA: " + schema);
		// Altera o schema do banco para acessar
		schemaController.alterarUsuario(schema);
		cliente.setSchema(schema);

		log.info("SCHEMA ATUAL: " + schema);
		if (usuario.getNome() != null) {
			if (usuario.getNome().length() > 20) {
				usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getNome().substring(0, 19)));
			} else {
				usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getNome()));
			}
		} else {
			usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getEmail().substring(0, 19)));
		}
		log.info("Fazendo busca de FR_USUARIO nos Schemas!");
		if (schemaController.buscarIdGfsisUsuario(usuario.getIdGfsis(), cliente.getSchema()) == null) {
			log.info("FR_USUARIO não encontrado! Cadastrando novo FR_USUARIO!");
			schemaController.cadastrarUsuario(usuario.getLogin(), usuario.getSenha(), "N", usuario.getNome(),
					usuario.getEmail(), usuario.getIdGfsis(), cliente.getSchema());
		}
		return ResponseEntity.ok().body(usuario);
	}

	public ResponseEntity<Usuario> atualizaUsuario(Cliente newCliente, Usuario oldUsuario, String schema)
			throws ErroService {
		boolean verificaEmail = usuarioService.findByEmail(newCliente.getEmail());

		if (verificaEmail && !newCliente.getEmail().equals(oldUsuario.getEmail())) {
			log.warn("Novo email ja existe, não será atualizado os dados!");
			return ResponseEntity.ok().body(null);
		}

		try {
			Usuario usuario = new RetornaUsuarioEndPoint().atualizaUsuario(newCliente, oldUsuario);
			String antigoLogin = oldUsuario.getLogin();
			usuario.setMensagem(1);

			if (usuario.getNome() != null) {
				if (usuario.getNome().length() > 20) {
					usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getNome().substring(0, 19)));
				} else {
					usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getNome()));
				}
			} else {
				usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getEmail().substring(0, 19)));
			}
			log.info("Login do usuário atualizado: " + usuario.getLogin());
			usuarioService.persistir(usuario);

			SchemaController schemaController = new SchemaController();
			if (schema == null) {
				log.info("CRIANDO NOVO SCHEMA...");
				schema = schemaController.criaSchema();
			}
			log.info("SCHEMA: " + schema);
			schemaController.alterarUsuario(schema);
			newCliente.setSchema(schema);

			if (!usuario.getLogin().equals(antigoLogin)) {
				SchemaCadEmpresaLogin schemaCadEmpresaLogin = new SchemaCadEmpresaLogin();
				schemaCadEmpresaLogin.atualizarCadEmpresaLogin(usuario.getLogin(), antigoLogin, schema);
			}

			Long idUsuario = schemaController.buscarIdGfsisUsuario(usuario.getIdGfsis(), newCliente.getSchema());
			if (idUsuario == null) {
				schemaController.cadastrarUsuario(usuario.getLogin(), usuario.getSenha(), "N", usuario.getNome(),
						usuario.getEmail(), usuario.getIdGfsis(), newCliente.getSchema());
				idUsuario = schemaController.buscarIdGfsisUsuario(usuario.getIdGfsis(), newCliente.getSchema());
			} else
				schemaController.atualizarUsuario(idUsuario, usuario.getLogin(), "N", usuario.getNome(),
						usuario.getEmail(), newCliente.getSchema());

			return ResponseEntity.ok().body(usuario);
		} catch (Exception e) {
			log.warn("Erro ao alterar usuário!");
			return ResponseEntity.ok().body(null);
		}
	}

	public ResponseEntity<UsuarioCliente> cadastraUsuarioCliente(Long idUsuario, Long idCliente) throws ErroService {
		RetornaUsuarioClienteEndPoint usuarioEndPoint = new RetornaUsuarioClienteEndPoint();
		UsuarioCliente usuarioCliente = usuarioEndPoint.cadastraUsuarioCliente(idUsuario, idCliente);
		return ResponseEntity.ok().body(usuarioCliente);
	}

	public ResponseEntity<UsuarioCliente> atualizaUsuarioCliente(Long idCliente, Long IdUsuario, Long oldIdUser)
			throws ErroService {
		UsuarioCliente oldUsuarioCliente = usuarioClienteService.findByIdUsuario(oldIdUser);
		UsuarioCliente usuarioCliente = new RetornaUsuarioClienteEndPoint().atualizaUsuarioCliente(idCliente, IdUsuario,
				oldUsuarioCliente);
		return ResponseEntity.ok().body(usuarioCliente);
	}
}