package br.com.avanteweb.a10.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchemaCadEmpresaLogin {
private static final Logger log = LoggerFactory.getLogger(SchemaCadEmpresaLogin.class);
    
    private DataBase bancoDados;

    public SchemaCadEmpresaLogin() {
        bancoDados = new DataBase();
//        bancoDados.defineEndereco("jdbc:oracle:thin:@localhost:1521:xe");
        bancoDados.defineEndereco("jdbc:oracle:thin:@a10new.couhp8fc9kia.sa-east-1.rds.amazonaws.com:1521:A10NEW");
//        bancoDados.defineEndereco("jdbc:oracle:thin:@172.27.0.24:1521:ORCL");
    }

    public boolean atualizarCadEmpresaLogin(String novoLogin, String antigoLogin, String schema) {
            
        log.info("SCHEMA - ATUALIZANDO CAD_EMPRESA_LOGIN...");
        String comando = "UPDATE "+schema+".CAD_EMPRESA_LOGIN SET LOGIN = '"+novoLogin+"' where login = '"
        		+ antigoLogin +"'";
        log.info(comando);
        bancoDados.abreConexao();
        if(!bancoDados.executaComando(comando)) {
            log.error("SCHEMA - ERRO UPDATE CAD_EMPRESA_LOGIN");
            bancoDados.fechaConexao();
            return false;
        }
        log.info("SCHEMA - CAD_EMPRESA_LOGIN ATUALIZADO");
        bancoDados.fechaConexao();
        return true;
    }
    
    public DataBase getBancoDados() {
        return bancoDados;
    }

    public void setBancoDados(DataBase bancoDados) {
        this.bancoDados = bancoDados;
    }
}
