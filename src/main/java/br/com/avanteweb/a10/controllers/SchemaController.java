package br.com.avanteweb.a10.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SchemaController
 */
public class SchemaController {

    private static final Logger log = LoggerFactory.getLogger(SchemaController.class);
    
    private DataBase bancoDados;

    public SchemaController() {
        bancoDados = new DataBase();
//        bancoDados.defineEndereco("jdbc:oracle:thin:@localhost:1521:xe");
        bancoDados.defineEndereco("jdbc:oracle:thin:@a10new.couhp8fc9kia.sa-east-1.rds.amazonaws.com:1521:A10NEW");
//        bancoDados.defineEndereco("jdbc:oracle:thin:@172.27.0.24:1521:ORCL");
    }
    public String novoSchema() {
        String schema = criaSchema();
        return schema;
    }

    public String criaSchema() {
        bancoDados.abreConexao();
        log.info("SCHEMA - BUSCANDO ULTIMO SCHEMA.");
        // String consulta = AvanteApiGfsisApplication.getSelectSchema();
        // bancoDados.executaConsulta(consulta);
        Integer novoSchema = bancoDados.retornaSchema();
        if(novoSchema != null) {
            log.info("SCHEMA - NOVO SCHEMA - A10_" + novoSchema);
            return "A10_"+novoSchema;
        }
        return null;
    }
    
    public boolean cadastrarUsuario(String login, String senha, String admin, String nome, String email, 
      Long idGfsis, String schema) {

        log.info("SCHEMA - SALVANDO FR_USUARIO.");
        String comando = "Insert into "+schema+".FR_USUARIO "
        +"(USR_LOGIN, USR_SENHA, USR_ADMINISTRADOR, USR_NOME, USR_EMAIL, ID_GFSIS, STATUS) values "
        +"('"+login+"','"+senha+"','"+admin+"','"+nome+"','"+email+"', "+idGfsis+" ,1)";
        log.info(comando);
        bancoDados.abreConexao();
        if(!bancoDados.executaComando(comando)) {
            log.error("SCHEMA - ERRO INSERT FR_USUARIO");
            bancoDados.fechaConexao();
            return false;
        }
        log.info("SCHEMA - FR_USUARIO SALVA");
        bancoDados.fechaConexao();
        return true;
    }

    public boolean atualizarUsuario(Long idUsuario, String login, String admin, String nome, String email, 
        String schema) {
            
        log.info("SCHEMA - ATUALIZANDO FR_USUARIO...");
        String comando = "UPDATE "+schema+".FR_USUARIO SET "
        +"USR_LOGIN = '"+login+"', USR_ADMINISTRADOR = '"+admin+"', "
        +"USR_NOME = '"+nome+"', USR_EMAIL = '"+email+"', STATUS = 1 "
        +"WHERE USR_CODIGO = "+idUsuario;
        log.info(comando);
        bancoDados.abreConexao();
        if(!bancoDados.executaComando(comando)) {
            log.error("SCHEMA - ERRO UPDATE FR_USUARIO");
            bancoDados.fechaConexao();
            return false;
        }
        log.info("SCHEMA - FR_USUARIO ATUALIZADO");
        bancoDados.fechaConexao();
        return true;
    }
  
    public Long buscarIdUsuario(String usrEmail, String schema) {
        
        log.info("SCHEMA - BUSCANDO FR_USUARIO...");
        String comando = "SELECT USR_CODIGO as CODIGO FROM "+schema+".FR_USUARIO "
        +"where USR_EMAIL = '"+usrEmail+"'";
        log.info(comando);
        bancoDados.abreConexao();
        bancoDados.executaConsulta(comando);
        if(bancoDados.proximoRegistro()) {
            String codigoUsuario = bancoDados.retornaString("CODIGO");
            log.info("SCHEMA - FR_USUARIO ENCONTRADO!");
            bancoDados.fechaConexao();
            Long codigoUsuarioAux = Long.valueOf(codigoUsuario);
            return codigoUsuarioAux;
        }
        log.info("SCHEMA - FR_USUARIO NAO ENCONTRADO");
        bancoDados.fechaConexao();
        return null;
    }

    public Long buscarIdGfsisUsuario(Long idGfsis, String schema) {
        
        log.info("SCHEMA - BUSCANDO FR_USUARIO...");
        String comando = "SELECT USR_CODIGO as CODIGO FROM "+schema+".FR_USUARIO "
        +"where ID_GFSIS = "+idGfsis;
        log.info(comando);
        if(idGfsis == null) {
            log.info("SCHEMA - FR_USUARIO NAO ENCONTRADO - ID_GFSIS NULL");
            return null;
        }
        bancoDados.abreConexao();
        bancoDados.executaConsulta(comando);
        if(bancoDados.proximoRegistro()) {
            String codigoUsuario = bancoDados.retornaString("CODIGO");
            log.info("SCHEMA - FR_USUARIO ENCONTRADO!");
            bancoDados.fechaConexao();
            Long codigoUsuarioAux = Long.valueOf(codigoUsuario);
            return codigoUsuarioAux;
        }
        log.info("SCHEMA - FR_USUARIO NAO ENCONTRADO");
        bancoDados.fechaConexao();
        return null;
    }
    
    public DataBase getBancoDados() {
        return bancoDados;
    }

    public void setBancoDados(DataBase bancoDados) {
        this.bancoDados = bancoDados;
    }

    public void alterarUsuario(String usuario) {
        this.bancoDados.setUsuario(usuario);
    }
	
}