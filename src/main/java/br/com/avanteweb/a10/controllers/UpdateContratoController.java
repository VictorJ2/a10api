package br.com.avanteweb.a10.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.avanteweb.a10.entities.Cliente;
import br.com.avanteweb.a10.entities.Contrato;
import br.com.avanteweb.a10.entities.Usuario;
import br.com.avanteweb.a10.entities.UsuarioCliente;
import br.com.avanteweb.a10.exception.services.ErroService;
import br.com.avanteweb.a10.models.gfsis.ContratoModelGfsis;
import br.com.avanteweb.a10.retorna.endpoints.RetornaContratoEndPoint;
import br.com.avanteweb.a10.retorna.endpoints.RetornaUsuarioEndPoint;
import br.com.avanteweb.a10.services.ClienteService;
import br.com.avanteweb.a10.services.ContratoService;
import br.com.avanteweb.a10.services.UsuarioClienteService;
import br.com.avanteweb.a10.services.UsuarioService;
import br.com.avanteweb.a10.utils.RemoveAcentos;

@RestController
@RequestMapping("/api/contrato") // ALTERAR
@CrossOrigin(origins = "*") // ALTERAR
public class UpdateContratoController {

	private static final Logger log = LoggerFactory.getLogger(UpdateContratoController.class);

	@Autowired
	private ContratoService contratoService;

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private UsuarioClienteService usuarioClienteService;

	public UpdateContratoController() {

	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<ContratoModelGfsis> atualizar(@PathVariable("id") Long idContrato,
			@Valid @RequestBody ContratoModelGfsis contratoModelGfsis, BindingResult result)
			throws NoSuchAlgorithmException, ErroService {

		log.info("\n");
		log.info("#########################################################################################");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### -------------------- ATUALIZAR SITUACAO CONTRATO GFSIS -------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("#########################################################################################");
		log.info("\nAtualizando Contrato Gfsis: {}", idContrato);

		Contrato newContrato = new RetornaContratoEndPoint().conversaoParaContrato(contratoModelGfsis);
		Contrato buscaContrato = contratoService.findContratoById(idContrato);
		if (buscaContrato != null) {
			Cliente buscaCliente = clienteService.findClientebyId(buscaContrato.getIdCliente());
			log.info("Dados do antigo Cliente: " + buscaCliente.toString());
			if (buscaCliente != null) {
				if (contratoModelGfsis.getSituacao().contains("Execução")) {
					buscaCliente.setInativa(0);
					buscaCliente.setExcluida(0);
				} else if (contratoModelGfsis.getSituacao().contains("Suspenso")
						|| contratoModelGfsis.getSituacao().contains("Inadimplente")) {
					buscaCliente.setInativa(1);
					buscaCliente.setExcluida(0);
				} else if (contratoModelGfsis.getSituacao().contains("Finalizado")) {
					buscaCliente.setInativa(1);
					buscaCliente.setExcluida(0);
					buscaCliente.setIdGfsis(null);
					List<UsuarioCliente> listaASerDeletada = usuarioClienteService.findByIdCliente(buscaCliente.getIdCliente());
					if(listaASerDeletada != null)
						for(UsuarioCliente userCli: listaASerDeletada) {
							usuarioService.deletarUsuarioById(userCli.getIdUsuario());
						}
					log.info("Lista de Usuários deletada");
					log.info("#########################################################################################\n");
//					contratoService.deleteContratoById(buscaContrato.getIdContrato());
//					log.info("Deletando Contrato");
					log.info("#########################################################################################\n");
					clienteService.persistir(buscaCliente);
					return ResponseEntity.ok().body(contratoModelGfsis);
				}

				clienteService.persistir(buscaCliente);
				newContrato = new RetornaContratoEndPoint().atualizaContrato(newContrato, buscaContrato);
				newContrato.setIdCliente(buscaCliente.getIdCliente());
				contratoService.persistir(newContrato);
				log.info("Contrato Atualizado");
				log.info("#########################################################################################\n");
			}
		} else {
			log.info("Dados do contrato: " + newContrato.toString());
			log.info("Situação nao atualizada");
		}
		return ResponseEntity.ok().body(contratoModelGfsis);
	}

	@PostMapping(value = "/situacao")
	public ResponseEntity<ContratoModelGfsis> atualizar(@Valid @RequestBody ContratoModelGfsis contratoModelGfsis,
			@RequestHeader(name = "token", required = false) String token, BindingResult result)
			throws NoSuchAlgorithmException, ErroService {

		log.info("\n");
		log.info("#########################################################################################");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### -------------------- ATUALIZAR SITUACAO CONTRATO GFSIS -------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("###### --------------------------------------------------------------------------- ######");
		log.info("#########################################################################################");
		log.info("\nAtualizando Estado do Contrato Gfsis: {}", contratoModelGfsis.toString());

		Contrato newContrato = new RetornaContratoEndPoint().conversaoParaContrato(contratoModelGfsis);
		Contrato buscaContrato = contratoService.findContratoByIdGfsis(contratoModelGfsis.getId());
		
		if(contratoModelGfsis.getSituacao().contains("Aguardando aceite")) {
			return null;
		}
		
		if (buscaContrato != null) {
			Cliente buscaCliente = clienteService.findClientebyId(buscaContrato.getIdCliente());
			
			//Caso em que pode existir contrato antigo do cliente e chega schema antigo finalizado
			//Tratando para nao modificar o cliente atual
			if(buscaCliente != null) {
				List<Contrato> contratosExistentes = contratoService.findContratoByIdCliente(buscaCliente.getIdCliente());
				Long idNaoExisteFinalizado = null;
				if(contratosExistentes != null) {
					for(Contrato aux: contratosExistentes) {
						if(!aux.getSituacao().contains("Finalizado")) {
							idNaoExisteFinalizado = aux.getIdGfsis();
							break;
						}
					}
				
					if(idNaoExisteFinalizado != null &&
							idNaoExisteFinalizado != contratoModelGfsis.getId() && 
									contratoModelGfsis.getSituacao().contains("Finalizado")) {
						log.info("Ja existe contrato diferente de finalisado e chegou finalizado, processo termina aqui!");
						return null;
					}
				}
			}
			
			
			if (buscaCliente != null) {
				if (contratoModelGfsis.getSituacao().equals("Em execução")) {
					buscaCliente.setInativa(0);
					buscaCliente.setExcluida(0);
				} else if (contratoModelGfsis.getSituacao().equals("Suspenso")
						|| contratoModelGfsis.getSituacao().equals("Inadimplente")) {
					buscaCliente.setInativa(1);
					buscaCliente.setExcluida(0);
				} else if (contratoModelGfsis.getSituacao().equals("Finalizado")) {
					buscaCliente.setInativa(1);
					buscaCliente.setExcluida(0);
					buscaCliente.setIdGfsis(null);
					List<UsuarioCliente> listaASerDeletada = usuarioClienteService.findByIdCliente(buscaCliente.getIdCliente());
					if(listaASerDeletada != null)
						for(UsuarioCliente userCli: listaASerDeletada) {
							usuarioService.deletarUsuarioById(userCli.getIdUsuario());
						}
					log.info("Lista de Usuários deletada");
					log.info("#########################################################################################\n");
//					contratoService.deleteContratoById(buscaContrato.getIdContrato());
//					log.info("Deletando Contrato");
					buscaContrato.setIdGfsis(null);
					log.info("Setado null no IdGFSIS");
					log.info("#########################################################################################\n");
				}
				clienteService.persistir(buscaCliente);
				newContrato = new RetornaContratoEndPoint().atualizaContrato(newContrato, buscaContrato);
				newContrato.setIdCliente(buscaCliente.getIdCliente());
				contratoService.persistir(newContrato);
				log.info("Contrato Atualizado");
				log.info("#########################################################################################\n");
			}
		} else {
			log.info("Situação nao atualizada");
		}
		return ResponseEntity.ok().body(contratoModelGfsis);
	}

	public ResponseEntity<Usuario> atualizaUsuario(Cliente newCliente, Usuario oldUsuario, String schema)
			throws ErroService {

		try {
			Usuario usuario = new RetornaUsuarioEndPoint().atualizaUsuario(newCliente, oldUsuario);
			String antigoLogin = oldUsuario.getLogin();

			if (usuario.getNome() != null) {
				if (usuario.getNome().length() > 20) {
					usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getNome().substring(0, 19)));
				} else {
					usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getNome()));
				}
			} else {
				usuario.setLogin(RemoveAcentos.removeAcentos(usuario.getEmail().substring(0, 19)));
			}
			log.info("Login do usuário atualizado: " + usuario.getLogin());
			usuarioService.persistir(usuario);

			SchemaController schemaController = new SchemaController();
			log.info("CRIANDO NOVO SCHEMA...");
			if (schema == null) {
				schema = schemaController.criaSchema();
			}
			log.info("SCHEMA: " + schema);
			schemaController.alterarUsuario(schema);
			newCliente.setSchema(schema);

			if (!usuario.getLogin().equals(antigoLogin)) {
				SchemaCadEmpresaLogin schemaCadEmpresaLogin = new SchemaCadEmpresaLogin();
				schemaCadEmpresaLogin.atualizarCadEmpresaLogin(usuario.getLogin(), antigoLogin, schema);
			}

			Long idUsuario = schemaController.buscarIdGfsisUsuario(usuario.getIdGfsis(), newCliente.getSchema());
			if (idUsuario == null) {
				schemaController.cadastrarUsuario(usuario.getLogin(), usuario.getSenha(), "N", usuario.getNome(),
						usuario.getEmail(), usuario.getIdGfsis(), newCliente.getSchema());
				idUsuario = schemaController.buscarIdGfsisUsuario(usuario.getIdGfsis(), newCliente.getSchema());
			} else
				schemaController.atualizarUsuario(idUsuario, usuario.getLogin(), "N", usuario.getNome(),
						usuario.getEmail(), newCliente.getSchema());

			return ResponseEntity.ok().body(usuario);
		} catch (Exception e) {
			log.warn("Erro ao alterar usuário!");
			return ResponseEntity.ok().body(null);
		}
	}
}