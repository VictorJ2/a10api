package br.com.avanteweb.a10.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity
@Table(name = "CLIENTE")
public class Cliente implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(sequenceName="CLIENTE_SEQ", name = "CLIENTE_SEQ", allocationSize = 1)
    @Id @GeneratedValue(generator="CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @Column(name = "ID_CLIENTE", nullable = false)
	private Long idCliente;
	@Size(max = 120)
    @Column(name="RAZAO_SOCIAL")
	private String razao_social;
	@Size(max = 120)
    @Column(name="NOME_FANTASIA")
	private String nome_fantasia;
	@Size(max = 120)
    @Column(name="ENDERECO")
	private String endereco;
    @Column(name="NUMERO")
	private String numero;
	@Size(max = 100)
    @Column(name="COMPLEMENTO")
	private String complememto;
	@Size(max = 100)
    @Column(name="BAIRRO")
	private String bairro;
	@Size(max = 120)
    @Column(name="MUNICIPIO")
	private String municipio;
	@Size(max = 2)
    @Column(name="UF")
	private String UF;
	@Size(max = 10)
    @Column(name="CEP")
	private String cep;
	@Size(max = 13)
    @Column(name="TELEFONE")
	private String telefone;
	@Size(max = 14)
    @Column(name="CNPJ_CPF")
	private String cnpj_cpf;
	@Size(max = 25)
    @Column(name="INSCRICAO_ESTADUAL")
	private String inscricao_estadual;
	@Size(max = 25)
    @Column(name="INSCRICAO_MUNICIPAL")
	private String inscricao_municipal;
	@Size(max = 120)
    @Column(name="EMAIL", nullable = false)
	private String email;
    @Column(name="EXCLUIDA")
	private int excluida;
    @Column(name="INATIVA")
	private int inativa;
    @Size(max = 150)
    @Column(name="SCHEMA")
	private String schema;
    @Column(name="DATA_CADASTRO")
	private Date dataCadastro;
    @Column(name="ID_GFSIS")
    private Long idGfsis;
    @Size(max = 50)
    @Column(name="PLANO_ICMS")
    private String planoIcms;
    @Size(max = 50)
    @Column(name="PLANO_ISS")
    private String planoIss;
    @Column(name="PLANO_DATA_INICIO_ICMS")
    private Date planoDataInicioIcms;
    @Column(name="PLANO_DATA_FIM_ICMS")
    private Date planoDataFimIcms;
    @Column(name="PLANO_DATA_INICIO_ISS")
    private Date planoDataInicioIss;
    @Column(name="PLANO_DATA_FIM_ISS")
    private Date planoDataFimIss;
    @Column(name="LINK_PAG_GFSIS")
    private String urlGfsisi;
    
    
	public Long getIdGfsis() {
		return idGfsis;
	}

	public void setIdGfsis(Long idGfsis) {
		this.idGfsis = idGfsis;
	}

	public Cliente() {

	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getRazao_social() {
		return razao_social;
	}

	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}

	public String getNome_fantasia() {
		return nome_fantasia;
	}

	public void setNome_fantasia(String nome_fantasia) {
		this.nome_fantasia = nome_fantasia;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplememto() {
		return complememto;
	}

	public void setComplememto(String complememto) {
		this.complememto = complememto;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUF() {
		return UF;
	}

	public void setUF(String uF) {
		UF = uF;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getPlanoIcms() {
		return planoIcms;
	}
	
	public String getPlanoIss() {
		return planoIss;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCnpj_cpf() {
		return cnpj_cpf;
	}

	public void setCnpj_cpf(String cnpj_cpf) {
		this.cnpj_cpf = cnpj_cpf;
	}

	public String getInscricao_estadual() {
		return inscricao_estadual;
	}

	public void setInscricao_estadual(String inscricao_estadual) {
		this.inscricao_estadual = inscricao_estadual;
	}

	public String getInscricao_municipal() {
		return inscricao_municipal;
	}

	public void setInscricao_municipal(String inscricao_municipal) {
		this.inscricao_municipal = inscricao_municipal;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getExcluida() {
		return excluida;
	}

	public void setExcluida(int excluida) {
		this.excluida = excluida;
	}

	public int getInativa() {
		return inativa;
	}

	public void setInativa(int inativa) {
		this.inativa = inativa;
	}

	public String getSchema() {
		return schema;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}
	
	public void setPlanoIcms(String planoIcms) {
		this.planoIcms = planoIcms;
	}
	
	public void setPlanoIss(String planoIss) {
		this.planoIss = planoIss;
	}
	
	public Date getPlanoDataFimIcms() {
		return planoDataFimIcms;
	}
	
	public Date getPlanoDataFimIss() {
		return planoDataFimIss;
	}
	
	public Date getPlanoDataInicioIcms() {
		return planoDataInicioIcms;
	}

	public Date getPlanoDataInicioIss() {
		return planoDataInicioIss;
	}

	public void setPlanoDataInicioIss(Date planoDataInicioIss) {
		this.planoDataInicioIss = planoDataInicioIss;
	}

	public void setPlanoDataInicioIcms(Date planoDataInicioIcms) {
		this.planoDataInicioIcms = planoDataInicioIcms;
	}

	public void setPlanoDataFimIcms(Date planoDataFimIcms) {
		this.planoDataFimIcms = planoDataFimIcms;
	}

	public void setPlanoDataFimIss(Date planoDataFimIss) {
		this.planoDataFimIss = planoDataFimIss;
	}
	
	public String getUrlGfsisi() {
		return urlGfsisi;
	}
	
	public void setUrlGfsisi(String urlGfsisi) {
		this.urlGfsisi = urlGfsisi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCliente == null) ? 0 : idCliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (idCliente == null) {
			if (other.idCliente != null)
				return false;
		} else if (!idCliente.equals(other.idCliente))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cliente [idCliente=" + idCliente + ", razao_social=" + razao_social + ", nome_fantasia=" + nome_fantasia
				+ ", endereco=" + endereco + ", numero=" + numero + ", complememto=" + complememto + ", bairro="
				+ bairro + ", municipio=" + municipio + ", UF=" + UF + ", cep=" + cep + ", telefone=" + telefone
				+ ", cnpj_cpf=" + cnpj_cpf + ", inscricao_estadual=" + inscricao_estadual + ", inscricao_municipal="
				+ inscricao_municipal + ", email=" + email + ", excluida=" + excluida + ", inativa=" + inativa
				+ ", schema=" + schema + ", dataCadastro=" + dataCadastro + ", idGfsis=" + idGfsis + ", planoIcms="
				+ planoIcms + ", planoIss=" + planoIss + ", planoDataInicioIcms=" + planoDataInicioIcms
				+ ", planoDataFimIcms=" + planoDataFimIcms + ", planoDataInicioIss=" + planoDataInicioIss
				+ ", planoDataFimIss=" + planoDataFimIss + ", urlGfsisi=" + urlGfsisi + "]";
	}
}
