package br.com.avanteweb.a10.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="CONTRATO")
public class Contrato implements Serializable {

    /**
     * Dados do contrato
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(sequenceName="CONTRATO_SEQ", name = "CONTRATO_SEQ", allocationSize = 1)
    @Id @GeneratedValue(generator="CONTRATO_SEQ", strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CONTRATO")
    private Long idContrato;
    @Size(max = 50)
    @Column(name="SITUACAO", nullable = false)
    private String situacao;
    @Column(name="DATA_INICIO")
    private Date dataInicio;
    @Column(name="DATA_FIM")
    private Date dataFim;
    @Column(name = "ID_CLIENTE")
    private Long idCliente;
    @Column(name = "ID_GFSIS")
    private Long idGfsis;

    public Long getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Long idContrato) {
        this.idContrato = idContrato;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }
    
    public Long getIdGfsis() {
        return idGfsis;
    }

    public void setIdGfsis(Long idGfsis) {
        this.idGfsis = idGfsis;
    }

    @Override
	public String toString() {
		return "Contrato [idContrato=" + idContrato + ", situacao=" + situacao + ", dataInicio=" + dataInicio
				+ ", dataFim=" + dataFim + ", idCliente=" + idCliente + ", idGfsis=" + idGfsis + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idContrato == null) ? 0 : idContrato.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contrato other = (Contrato) obj;
		if (idContrato == null) {
			if (other.idContrato != null)
				return false;
		} else if (!idContrato.equals(other.idContrato))
			return false;
		return true;
	}

    
}