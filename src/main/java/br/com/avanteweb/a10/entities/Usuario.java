package br.com.avanteweb.a10.entities;

import java.io.Serializable;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "USUARIO")
public class Usuario implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(sequenceName="USUARIO_SEQ", name = "USUARIO_SEQ", allocationSize = 1)
    @Id @GeneratedValue(generator="USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @Column(name = "ID_USUARIO", nullable = false)
	private Long idUsuario;
	@Size(max = 120)
    @Column(name="EMAIL")
	private String email;
	@Size(max = 120)
    @Column(name="NOME")
	private String nome;
	@Size(max = 120)
    @Column(name="LOGIN")
	private String login;
	@Size(max = 120)
    @Column(name="SENHA")
	private String senha;
    @Column(name="ID_GRUPO")
	private int grupo;
    @Column(name="STATUS")
	private Integer status;
    @Column(name = "ID_GFSIS")
    private Long idGfsis;
    @Column(name="MENSAGEM_USUARIO")
	private int mensagem;
	
	public Usuario() {
		// TODO Auto-generated constructor stub
		this.mensagem = 1;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getGrupo() {
		return grupo;
	}

	public void setGrupo(int grupo) {
		this.grupo = grupo;
	}

	public Long getIdGfsis() {
		return idGfsis;
	}

	public void setIdGfsis(Long idGfsis) {
		this.idGfsis = idGfsis;
	}
	
	public int getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(int mensagem) {
		this.mensagem = mensagem;
	}
	
	public Integer getStatus() {
		return status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idUsuario == null) ? 0 : idUsuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (idUsuario == null) {
			if (other.idUsuario != null)
				return false;
		} else if (!idUsuario.equals(other.idUsuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", email=" + email + ", nome=" + nome + ", login=" + login
				+ ", senha=" + senha + ", grupo=" + grupo + ", status=" + status + ", idGfsis=" + idGfsis + "]";
	}
}
