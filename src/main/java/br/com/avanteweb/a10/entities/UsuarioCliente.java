package br.com.avanteweb.a10.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="USUARIO_CLIENTE")
public class UsuarioCliente {
	@SequenceGenerator(sequenceName="USUARIO_CLIENTE_SEQ", name = "USUARIO_CLIENTE_SEQ", allocationSize = 1)
    @Id @GeneratedValue(generator="USUARIO_CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
	@Column(name="ID_USUARIO_CLIENTE")
	private Long idClienteUsuario;
	@JoinColumn(name = "ID_USUARIO")
	@Column(name = "ID_USUARIO")
	private Long idUsuario;
	@Column(name = "ID_CLIENTE")
   	private Long idCliente;
	
	public UsuarioCliente() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdClienteUsuario() {
		return idClienteUsuario;
	}

	public void setIdClienteUsuario(Long idClienteUsuario) {
		this.idClienteUsuario = idClienteUsuario;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idClienteUsuario == null) ? 0 : idClienteUsuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioCliente other = (UsuarioCliente) obj;
		if (idClienteUsuario == null) {
			if (other.idClienteUsuario != null)
				return false;
		} else if (!idClienteUsuario.equals(other.idClienteUsuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioCliente [idClienteUsuario=" + idClienteUsuario + ", idUsuario=" + idUsuario + ", idCliente="
				+ idCliente + "]";
	}
}
