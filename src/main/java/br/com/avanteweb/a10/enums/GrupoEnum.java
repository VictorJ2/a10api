package br.com.avanteweb.a10.enums;

public enum GrupoEnum {
	ROLE_ADMINISTRADOR(1),
	ROLE_CLIENTE(2); 
	
	private int tipo;
	
	private GrupoEnum(int i) {
		this.tipo = i;
	}
	
	public int getTipo() {
		return tipo;
	}
}
