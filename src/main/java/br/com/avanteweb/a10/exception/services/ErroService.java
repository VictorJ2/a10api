package br.com.avanteweb.a10.exception.services;

public class ErroService extends Exception{
	//Excessão criada, Excessao>Standard>ResourceException
		private static final long serialVersionUID = 1L;
		
		public ErroService(String msg) {
			super(msg);
		}
		
		public ErroService(String msg, Throwable erro) {
			super(msg, erro);
		}
}
