package br.com.avanteweb.a10.models.gfsis;

import java.io.Serializable;


public class ClienteModelGfsis implements Serializable {

    /**
     * Dados do cliente Gfsis
     */
    private static final long serialVersionUID = 1L;

    private Long id;
    private String cnpj;
    private String cpf;
    private String email;
    private String nome;
    private String nomeFantasia;
    private String crt;
    private EnderecoModelGfsis endereco;
    private GrupoModelGfsis grupo;
    private String urlAreaCliente;
    private String ie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public EnderecoModelGfsis getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoModelGfsis endereco) {
        this.endereco = endereco;
    }

    public GrupoModelGfsis getGrupo() {
        return grupo;
    }

    public void setGrupo(GrupoModelGfsis grupo) {
        this.grupo = grupo;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getCrt() {
        return crt;
    }

    public void setCrt(String crt) {
        this.crt = crt;
    }

    public String getLogradouro() {
        return endereco.getLogradouro();
    }

    public void setLogradouro(String logradouro) {
        endereco.setLogradouro(logradouro);
    }

    public String getNumero() {
        return endereco.getNumero();
    }

    public void setNumero(String numero) {
        endereco.setNumero(numero);
    }

    public String getComplemento() {
        return endereco.getComplemento();
    }

    public void setComplemento(String complemento) {
        endereco.setComplemento(complemento);
    }

    public String getBairro() {
        return endereco.getBairro();
    }

    public void setBairro(String bairro) {
        endereco.setBairro(bairro);
    }

    public String getCep() {
        return endereco.getCep();
    }

    public void setCep(String cep) {
        endereco.setCep(cep);
    }

    public String getMunicipio() {
        return endereco.getMunicipio();
    }

    public void setMunicipio(String municipio) {
        endereco.setMunicipio(municipio);
    }

    public String getUf() {
        return endereco.getUf();
    }

    public void setUf(String uf) {
        endereco.setUf(uf);
    }

    @Override
    public String toString() {
        return "ClienteModelGfsis [cnpj=" + cnpj + ", cpf=" + cpf + ", crt=" + crt + ", email=" + email + ", endereco="
                + endereco + ", grupo=" + grupo + ", id=" + id + ", inscricaoEstadual=" + ie + ", nome="
                + nome + ", nomeFantasia=" + nomeFantasia + ", urlAreaCliente=" + urlAreaCliente + "]";
    }

    public String getUrlAreaCliente() {
        return urlAreaCliente;
    }

    public void setUrlAreaCliente(String urlAreaCliente) {
        this.urlAreaCliente = urlAreaCliente;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    

    
}