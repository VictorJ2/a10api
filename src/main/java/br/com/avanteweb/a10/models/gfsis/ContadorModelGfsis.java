package br.com.avanteweb.a10.models.gfsis;

import java.io.Serializable;

public class ContadorModelGfsis implements Serializable {

    /**
     * Dados do Contador Gfsis
     */
    private static final long serialVersionUID = 1L;

    private Long id;
    private String nome;
    private String cnpj;
    private String cpf;
    private String email;
    private EnderecoModelGfsis endereco;
    private DistribuidorModelGfsis distribuidor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DistribuidorModelGfsis getDistribuidor() {
        return distribuidor;
    }

    public void setDistribuidor(DistribuidorModelGfsis distribuidor) {
        this.distribuidor = distribuidor;
    }

    @Override
    public String toString() {
        return "ContadorModelGfsis [cnpj=" + cnpj + ", cpf=" + cpf + ", distribuidor=" + distribuidor + ", email="
                + email + ", endereco=" + endereco + ", id=" + id + ", nome=" + nome + "]";
    }

    public EnderecoModelGfsis getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoModelGfsis endereco) {
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}