package br.com.avanteweb.a10.models.gfsis;

import java.io.Serializable;
import java.util.List;

public class ContratoModelGfsis implements Serializable {

    /**
     * Dados do Contrato Gfsis
     */
    private static final long serialVersionUID = 1L;

    private Long id;
    private String situacao;
    private String dataInicio;
    private String dataFim;
    private DistribuidorModelGfsis distribuidor;
    private ContadorModelGfsis contador;
    private ClienteModelGfsis cliente;
    private List<ItensModelGfsis> itens;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public DistribuidorModelGfsis getDistribuidor() {
        return distribuidor;
    }

    public void setDistribuidor(DistribuidorModelGfsis distribuidor) {
        this.distribuidor = distribuidor;
    }

    public ContadorModelGfsis getContador() {
        return contador;
    }

    public void setContador(ContadorModelGfsis contador) {
        this.contador = contador;
    }

    public ClienteModelGfsis getCliente() {
        return cliente;
    }

    public void setCliente(ClienteModelGfsis cliente) {
        this.cliente = cliente;
    }

    public List<ItensModelGfsis> getItens() {
        return itens;
    }

    public void setItens(List<ItensModelGfsis> itens) {
        this.itens = itens;
    }

    @Override
    public String toString() {
        return "ContratoModelGfsis [cliente=" + cliente + ", contador=" + contador + ", datafim=" + dataFim
                + ", datainicio=" + dataInicio + ", distribuidor=" + distribuidor + ", id=" + id + ", itens=" + itens
                + ", situacao=" + situacao + "]";
    }
    
}