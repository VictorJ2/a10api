package br.com.avanteweb.a10.models.gfsis;

import java.io.Serializable;

public class GrupoModelGfsis implements Serializable {

    /**
     * Dados do Grupo Gfsis
     */
    private static final long serialVersionUID = 1L;

    private Long id;
    private String nome;
    private String nomeFantasia;
    private String cnpj;
    private String cpf;
    private Long crt;
    private String ie;
    private String email;
    private String urlAreaCliente;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "GrupoModelGfsis [cnpj=" + cnpj + ", cpf=" + cpf + ", crt=" + crt + ", email=" + email + ", id=" + id
                + ", ie=" + ie + ", nome=" + nome + ", nomeFantasia=" + nomeFantasia + ", urlAreaCliente="
                + urlAreaCliente + "]";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public Long getCrt() {
        return crt;
    }

    public void setCrt(Long crt) {
        this.crt = crt;
    }

    public String getUrlAreaCliente() {
        return urlAreaCliente;
    }

    public void setUrlAreaCliente(String urlAreaCliente) {
        this.urlAreaCliente = urlAreaCliente;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }
}