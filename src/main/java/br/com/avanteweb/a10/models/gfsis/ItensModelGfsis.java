package br.com.avanteweb.a10.models.gfsis;

import java.io.Serializable;

public class ItensModelGfsis implements Serializable {

    /**
     * Dados do Itens Gfsis
     */
    private static final long serialVersionUID = 1L;

    private Integer id;
    private ProdutoModelGfsis produto;
    private String ie;
    private String observacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProdutoModelGfsis getProduto() {
        return produto;
    }

    public void setProduto(ProdutoModelGfsis produto) {
        this.produto = produto;
    }

    @Override
    public String toString() {
        return "ItensModelGfsis [id=" + id + ", ie=" + ie + ", observacao=" + observacao + ", produto=" + produto + "]";
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}