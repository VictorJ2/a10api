package br.com.avanteweb.a10.models.gfsis;

import java.io.Serializable;

public class ProdutoModelGfsis implements Serializable {

    /**
     * Dados do Itens Gfsis
     */
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String nome;
    private String dataInicio;
    private String dataFim;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	@Override
	public String toString() {
		return "ProdutoModelGfsis [id=" + id + ", nome=" + nome + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim
				+ "]";
	}
}