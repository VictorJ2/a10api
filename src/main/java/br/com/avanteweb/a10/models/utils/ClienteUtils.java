package br.com.avanteweb.a10.models.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avanteweb.a10.entities.Cliente;

public class ClienteUtils {
	private static final Logger log = LoggerFactory.getLogger(ClienteUtils.class);
	
	public Cliente ajusteSituaçãoClienteIcms(Cliente cliente, String planoIcms) {
		if(planoIcms.equals("Suspenso")) {
			cliente.setPlanoIcms(planoIcms);
			log.info("Contrato Icms Suspenso!!!!!");
		}
		return cliente;
	}
	
	public Cliente ajusteSituaçãoClienteIss(Cliente cliente, String planoIss) {
		if(planoIss.equals("Suspenso")) {
			cliente.setPlanoIcms(planoIss);
			log.info("Contrato Iss Suspenso!!!!!");
		}
		return cliente;
	}
}
