package br.com.avanteweb.a10.models.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avanteweb.a10.utils.RemoveAcentos;

public class ItemUtils {
	private static final Logger log = LoggerFactory.getLogger(ItemUtils.class);

	public String pegaNomeEmail(String email) {
		String saida = null;
		
		try {
			String[] vetorNome = email.split("@");
			if(vetorNome[0].contains("'")) {
				vetorNome[0] = vetorNome[0].replace("'", "");
			}
			
			saida = RemoveAcentos.removeAcentos(vetorNome[0].trim());
			return saida;
		} catch (Exception e) {
			log.warn("Erro ao obter nome do usuário a partir do email!");
		}
		return saida;
	}
}
