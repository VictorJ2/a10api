package br.com.avanteweb.a10.models.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avanteweb.a10.models.gfsis.ClienteModelGfsis;
import br.com.avanteweb.a10.models.gfsis.ContratoModelGfsis;

public class VerificacaoCampos {
	private static final Logger log = LoggerFactory.getLogger(VerificacaoCampos.class);
	
	public boolean verificaCamposObrigatorios(ContratoModelGfsis contrato) {
		ClienteModelGfsis cliente = contrato.getCliente();
		
		if(contrato.getId() == null) {
			log.info("ID do contrato veio Nulo: " +contrato.getId());
			return false;
		}
		
		if(contrato.getSituacao() == null || contrato.getSituacao().equals("")) {
			log.info("Situação do contrato veio Nulo ou vazio: " +contrato.getSituacao());
			return false;
		}
		
		if(cliente.getId() == null) {
			log.info("ID GFSIS do cliente veio Nulo");
			return false;
		}
		
		if(cliente.getNome() == null || cliente.getNome().equals("")) {
			log.info("ID do cliente veio Nulo: " +cliente.getNome());
			return false;
		}
		
		if((cliente.getCnpj() == null && cliente.getCpf() == null) || (cliente.getCpf().equals("") && cliente.getCnpj().equals(""))) {
			log.info("ID do cliente veio Nulo: " +cliente.getCnpj() +", " +cliente.getCpf());
			return false;
		}
		
		if(cliente.getEmail() == null || cliente.getEmail().equals("")) {
			log.info("ID do cliente veio Nulo: " +cliente.getEmail());
			return false;
		}
		
		if(cliente.getEndereco() == null) {
			log.info("ID do cliente veio Nulo: " +cliente.getEndereco().toString());
			return false;
		}
		
		return true;
	}

}
