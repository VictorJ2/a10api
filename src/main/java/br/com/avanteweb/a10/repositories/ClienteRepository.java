package br.com.avanteweb.a10.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.avanteweb.a10.entities.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long>{
	
	public Cliente findByIdGfsis(Long idGfsis);
	
	@Query(value = "SELECT CODIGO_IBGE from Cidade where UPPER(translate(NOME_CIDADE,'ÁÇÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕËÜáçéíóúàèìòùâêîôûãõëü','ACEIOUAEIOUAEIOUAOEUaceiouaeiouaeiouaoeu')) " +
   " = upper((translate(:nome,'ÁÇÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕËÜáçéíóúàèìòùâêîôûãõëü','ACEIOUAEIOUAEIOUAOEUaceiouaeiouaeiouaoeu'))) and UF = :estado", nativeQuery = true)
	String findNumeroIBGE(@Param("nome") String cidade, @Param("estado") String estado);
}
