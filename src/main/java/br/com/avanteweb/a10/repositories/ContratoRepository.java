package br.com.avanteweb.a10.repositories;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.avanteweb.a10.entities.Contrato;

@Configuration
@Repository
public interface ContratoRepository extends CrudRepository<Contrato, Long> {
	public Contrato findByIdGfsis(Long idGfsis);
	
	public List<Contrato> findByIdCliente(Long idGfsis);
}
