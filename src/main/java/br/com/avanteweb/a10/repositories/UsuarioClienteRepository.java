package br.com.avanteweb.a10.repositories;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.avanteweb.a10.entities.UsuarioCliente;

@Configuration
@Repository
public interface UsuarioClienteRepository extends CrudRepository<UsuarioCliente, Long> {
	public UsuarioCliente findByIdCliente(Long idCliente);
	
	public UsuarioCliente findByIdUsuario(Long idUsuario);
	
}
