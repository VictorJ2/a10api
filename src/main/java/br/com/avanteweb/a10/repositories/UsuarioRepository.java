package br.com.avanteweb.a10.repositories;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.avanteweb.a10.entities.Usuario;

@Configuration
@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
	Usuario findByIdGfsis(Long idGfsis);
	
	Usuario findByEmail(String email);
}
