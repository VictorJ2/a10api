package br.com.avanteweb.a10.retorna.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avanteweb.a10.entities.Cliente;
import br.com.avanteweb.a10.entities.Contrato;
import br.com.avanteweb.a10.exception.services.ErroService;
import br.com.avanteweb.a10.models.gfsis.ClienteModelGfsis;

public class RetornaClienteEndPoint {
	private static final Logger log = LoggerFactory.getLogger(RetornaClienteEndPoint.class);
	
	public Cliente conversaoParaCliente(ClienteModelGfsis clienteModel) throws ErroService {
		Cliente cliente = new Cliente();
		try {
			cliente.setIdGfsis(clienteModel.getId());
			//Trecho de código para requisição do nome
			if(clienteModel.getNome().trim().length() > 100) {
				if(clienteModel.getNome().contains("'")) {
					cliente.setRazao_social(clienteModel.getNome().trim().substring(0, 99).trim().replace("'", ""));
				}else {
					cliente.setRazao_social(clienteModel.getNome().trim().substring(0, 99).trim());
				}
			} else {
				if(clienteModel.getNome().contains("'")) {
					cliente.setRazao_social(clienteModel.getNome().trim().trim().replace("'", ""));
				}else {
					cliente.setRazao_social(clienteModel.getNome().trim().trim());
				}
			}
			
			if(clienteModel.getNomeFantasia() != null) {
				cliente.setNome_fantasia(clienteModel.getNomeFantasia().replaceAll("'", "").trim());
			}
			
			if(clienteModel.getCnpj()!= "") {
				cliente.setCnpj_cpf(clienteModel.getCnpj());
			}else {
				cliente.setCnpj_cpf(clienteModel.getCpf());
			}
			
			if(clienteModel.getEmail() != null) {
				cliente.setEmail(clienteModel.getEmail().replaceAll("'", "").trim());
			}
			if(clienteModel.getLogradouro() != null)
				cliente.setEndereco(clienteModel.getLogradouro().replaceAll("'", "").trim());
			
			if(clienteModel.getNumero() != null)
				cliente.setNumero(clienteModel.getNumero().replaceAll("'", "").trim());
			
			if(clienteModel.getComplemento() != null)
				cliente.setComplememto(clienteModel.getComplemento().replaceAll("'", "").trim());
			
			if(clienteModel.getBairro() != null)
				cliente.setBairro(clienteModel.getBairro().replaceAll("'", "").trim());
			
			if(clienteModel.getUrlAreaCliente() != null && !clienteModel.getUrlAreaCliente().contains("null"))
				cliente.setUrlGfsisi(clienteModel.getUrlAreaCliente());
			
			if(clienteModel.getCep() != null)
				cliente.setCep(clienteModel.getCep().replaceAll("'", "").trim());
			
			if(clienteModel.getMunicipio() != null)
				cliente.setMunicipio(clienteModel.getMunicipio().replaceAll("'", "").trim());
			
			cliente.setUF(clienteModel.getUf());
			
			if(clienteModel.getIe() == null || clienteModel.getIe().trim().equals("")) {
				cliente.setInscricao_estadual("ISENTO");
			}else if(clienteModel.getIe().trim().equalsIgnoreCase("ISENTO")) {
				cliente.setInscricao_estadual(clienteModel.getIe().trim());
			}else {
				cliente.setInscricao_estadual(clienteModel.getIe().replaceAll("'", "").trim());
			}
			
			cliente.setInscricao_municipal(null);
			cliente.setTelefone(null);
			cliente.setExcluida(0);
			cliente.setInativa(0);
			
			cliente.setSchema(null);
			log.info("Cliente extraido da requisição com sucesso! Dados: "+cliente.toString());
			return cliente;
		} catch (Exception e) {
			log.warn("Erro ao obter dados do Cliente!");
			return null;
		}
		
	}
	
	public Cliente atualizaCliente(Cliente newCliente, Cliente oldCliente) {
		try {
			Long id = oldCliente.getIdCliente();
			String schema = oldCliente.getSchema();
			oldCliente = newCliente;
			oldCliente.setSchema(schema);
			oldCliente.setIdCliente(id);
			log.info("Dados alterados do Cliente com sucesso! Dados: "+oldCliente.toString());
			return oldCliente;
		} catch (Exception e) {
			log.warn("Erro ao obter novos dados do Cliente!");
			return null;
		}
	}
	
	public Cliente atualizaSituacaoCliente(Contrato contrato, Cliente cliente) {
		if (contrato.getSituacao().equals("Em execução")) {
			cliente.setInativa(0);
			cliente.setExcluida(0);
		} else if (contrato.getSituacao().equals("Suspenso")
				|| contrato.getSituacao().equals("Inadimplente")) {
			cliente.setInativa(1);
			cliente.setExcluida(0);
		} else if (contrato.getSituacao().equals("Finalizado")) {
			cliente.setInativa(1);
			cliente.setExcluida(1);
		}
		return cliente;
	}
}
