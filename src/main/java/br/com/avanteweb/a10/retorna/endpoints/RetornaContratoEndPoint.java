package br.com.avanteweb.a10.retorna.endpoints;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avanteweb.a10.entities.Contrato;
import br.com.avanteweb.a10.models.gfsis.ContratoModelGfsis;

public class RetornaContratoEndPoint {
	private static final Logger log = LoggerFactory.getLogger(RetornaContratoEndPoint.class);
	
	public Contrato conversaoParaContrato(ContratoModelGfsis contrato) {
		try {
			Contrato contratoNew = new Contrato(); 
			SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
			try {
				contratoNew.setDataInicio(data.parse(contrato.getDataInicio()));
				if(contrato.getDataFim() != null) {
					contratoNew.setDataFim(data.parse(contrato.getDataFim()));
				}
			} catch (Exception e) {
			
			}
			contratoNew.setIdContrato(null);
			contratoNew.setIdCliente(null);
			contratoNew.setIdGfsis(contrato.getId());
			contratoNew.setSituacao(contrato.getSituacao());
			log.info("Dados do contrato obtido com sucesso! Dados: "+contratoNew.toString());
			return contratoNew;
		} catch (Exception e) {
			log.warn("Erro ao obter dados do Contrato!");
			return null;
		}
		
	}
	
	public Contrato atualizaContrato(Contrato newContrato, Contrato oldContrato) {
		try {
			Long id = oldContrato.getIdContrato(); 
			Long idCliente = oldContrato.getIdCliente();
			Long idGfsis = oldContrato.getIdGfsis();
			newContrato.setIdCliente(idCliente);
			newContrato.setIdContrato(id);
			newContrato.setIdGfsis(idGfsis);
			log.info("Dados do contrato obtidos e atualizados com sucesso. Dados: "+newContrato.toString());
			return newContrato;
		} catch (Exception e) {
			log.warn("Erro ao obter novos dados do contrato!");
			return null;
			// TODO: handle exception
		}
	}
}
