package br.com.avanteweb.a10.retorna.endpoints;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avanteweb.a10.models.gfsis.ItensModelGfsis;

public class RetornaItensEndPoint implements Serializable {
	private static final Logger log = LoggerFactory.getLogger(RetornaItensEndPoint.class);
	private static final long serialVersionUID = 1L;

	public List<String> retornaPlanoIss(List<ItensModelGfsis> listaItens) {
		List<String> plano = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		for (ItensModelGfsis item : listaItens) {

			Date dataPlanoInicio = null;
			Date dataPlanoFim = null;
			try {
				dataPlanoInicio = sdf.parse(item.getProduto().getDataInicio());
				if (item.getProduto().getDataFim() != null
						&& sdf.parse(item.getProduto().getDataFim()).getTime() > new Date().getTime()) {
					dataPlanoFim = sdf.parse(item.getProduto().getDataFim());
				}
			} catch (Exception e) {
				log.info("Erro ao obtem data inicio ou fim do plano ISS");
				return null;
			}

			if (item.getProduto().getNome().contains("A10 e-ISS Start")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Experience")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Fit")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Gold")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Premium 1")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Premium 2")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Premium 3")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Premium 4")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Premium 5")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Premium 6")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Premium 7")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ISS Premium")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			}

		}
		log.info("O plano e-ISS obtido foi: " + plano.toString());
		return plano;
	}

	public List<String> retornaPlanoIcms(List<ItensModelGfsis> listaItens) {
		List<String> plano = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		for (ItensModelGfsis item : listaItens) {

			Date dataPlanoInicio = null;
			Date dataPlanoFim = null;
			try {
				dataPlanoInicio = sdf.parse(item.getProduto().getDataInicio());
				if (item.getProduto().getDataFim() != null
						&& sdf.parse(item.getProduto().getDataFim()).getTime() > new Date().getTime()) {
					dataPlanoFim = sdf.parse(item.getProduto().getDataFim());
				}
			} catch (Exception e) {
				log.info("Erro ao obtem data inicio ou fim do plano ICMS");
				return null;
			}

			if (item.getProduto().getNome().equals("A10 Start")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().equals("A10 Fit")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 Experience")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Experience")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			}else if (item.getProduto().getNome().contains("A10 e-ICMS Play")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			}else if (item.getProduto().getNome().contains("A10 e-ICMS Start")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Fit")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Gold")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Premium 1")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Premium 2")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Premium 3")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Premium 4")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Premium 5")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Premium 6")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Premium 7")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Premium")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Company 1")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Company 2")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Company 3")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			} else if (item.getProduto().getNome().contains("A10 e-ICMS Company 4")
					&& (item.getProduto().getDataFim() == null || dataPlanoFim != null)) {
				if (plano.isEmpty()) {
					plano.add(item.getProduto().getNome().trim());
					plano.add(sdf.format(dataPlanoInicio));
					plano.add(dataPlanoFim != null ? sdf.format(dataPlanoFim) : null);
				} else {
					log.info("Plano duplicado: primeiro plano ->" + plano.get(0) + " data fim de : " + plano.get(2)
							+ " segundo plano -> " + item.getProduto().getNome() + " data fim de: "
							+ item.getProduto().getDataFim());
					return null;
				}
			}
		}
		log.info("O plano e-ICMS obtido foi: " + plano.toString());
		return plano;
	}
}
