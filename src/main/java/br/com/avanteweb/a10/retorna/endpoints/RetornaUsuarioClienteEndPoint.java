package br.com.avanteweb.a10.retorna.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avanteweb.a10.entities.UsuarioCliente;
import br.com.avanteweb.a10.exception.services.ErroService;

public class RetornaUsuarioClienteEndPoint {
	private static final Logger log = LoggerFactory.getLogger(RetornaUsuarioClienteEndPoint.class);

	
	public UsuarioCliente cadastraUsuarioCliente(Long idUsuario, Long idCliente) {
		try {
			UsuarioCliente usuarioCliente = new UsuarioCliente();
			usuarioCliente.setIdCliente(idCliente);
			usuarioCliente.setIdUsuario(idUsuario);
			log.info("Dados de UsuarioCliente coletados com sucesso! Dados: "+usuarioCliente.toString());
			return usuarioCliente;
		} catch (Exception e) {
			log.warn("Erro ao coletar dados de UsuarioCliente");
			return null;
		}
	}
	
	public UsuarioCliente atualizaUsuarioCliente(Long newIdCliente, Long newIdUsuario, UsuarioCliente old) throws ErroService {
		try {
			old.setIdCliente(newIdCliente);
			old.setIdUsuario(newIdUsuario);
			log.info("Dados de USUARIOCLIENTE atualizados com sucesso! Dados: "+ old.toString());
			return old;
		} catch (Exception e) {
			log.warn("Erro ao atualizar dados de cliente_usuario!");
			return null;
		}
		
	}
}
