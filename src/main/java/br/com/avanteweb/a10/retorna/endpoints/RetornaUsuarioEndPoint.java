package br.com.avanteweb.a10.retorna.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avanteweb.a10.entities.Cliente;
import br.com.avanteweb.a10.entities.Usuario;
import br.com.avanteweb.a10.enums.GrupoEnum;
import br.com.avanteweb.a10.models.utils.ItemUtils;
import br.com.avanteweb.a10.utils.PasswordUtil;

public class RetornaUsuarioEndPoint {
	private static final Logger log = LoggerFactory.getLogger(RetornaUsuarioEndPoint.class);

	
	public Usuario conversorParaUsuario(Cliente cliente) {
		Usuario user = new Usuario();
		ItemUtils itemUtils = new ItemUtils();
		if(itemUtils.pegaNomeEmail(cliente.getEmail()) == null) {
			return null;
		}
		
		try {
			user.setNome(itemUtils.pegaNomeEmail(cliente.getEmail()));

			if(cliente.getEmail().contains("'")) {
				user.setEmail(cliente.getEmail().replace("'", "").trim());
				user.setLogin(cliente.getEmail().replace("'", "").trim());
			} else {
				user.setEmail(cliente.getEmail().trim());
				user.setLogin(cliente.getEmail().trim());
			}
			
			user.setStatus(1);
			user.setIdGfsis(cliente.getIdGfsis());
			user.setSenha(null);
			user.setGrupo(GrupoEnum.ROLE_CLIENTE.getTipo());
			
			log.info("Dados do usaurio obtidos com sucesso! Dados: "+user.toString());
			return user;
		} catch (Exception e) {
			log.warn("Erro ao converter dados para usuario");
			return null;
		}
	}
		
	public Usuario atualizaUsuario(Cliente newUsuario, Usuario oldUsuario) {
		try {
			log.info("Antigo usuário: "+ oldUsuario.toString());
			Long id = oldUsuario.getIdUsuario();
			String senha = oldUsuario.getSenha();
			Usuario userAux = this.conversorParaUsuario(newUsuario);
			userAux.setIdUsuario(id);
			userAux.setSenha(senha);
			
			if(!userAux.getEmail().equals(oldUsuario.getEmail())){
				userAux.setSenha(PasswordUtil.gerarBCrypt(userAux.getIdUsuario(), "123"));
			}
			userAux.setStatus(1);
			log.info("Usuario atualizado com sucesso! Dados: "+userAux.toString());
			return userAux;
		} catch (Exception e) {
			log.warn("Erro ao atualizar usuario!");
			return null;
		}	
	}
}
