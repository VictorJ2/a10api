package br.com.avanteweb.a10.services;

import java.util.List;

import br.com.avanteweb.a10.entities.Cliente;
import br.com.avanteweb.a10.exception.services.ErroService;

public interface ClienteService {
	
	public Cliente persistir(Cliente cliente) throws ErroService;
	
	public void deletar(Cliente cliente) throws ErroService;
	
	public Cliente findClientebyId(Long IdCliente) throws ErroService;
	
	public Cliente findClienteByIdGfsis(Long id) throws ErroService;
		
	public List<Cliente> getAllClientes() throws ErroService;
}
