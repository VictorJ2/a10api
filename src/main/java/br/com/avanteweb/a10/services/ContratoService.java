package br.com.avanteweb.a10.services;

import java.util.List;

import br.com.avanteweb.a10.entities.Contrato;
import br.com.avanteweb.a10.exception.services.ErroService;

public interface ContratoService {
	
	public Contrato persistir(Contrato contrato) throws ErroService;

	public void deleteContratoById(Long idContrato) throws ErroService;

	public Contrato findContratoById(Long idContrato) throws ErroService;

	public Contrato findContratoByIdGfsis(Long idContrato) throws ErroService;
	
	public List<Contrato> findContratoByIdCliente(Long idCliente) throws ErroService;
	
    public List<Contrato> getAllContratos() throws ErroService;
    
    
}
