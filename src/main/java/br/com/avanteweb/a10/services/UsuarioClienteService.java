package br.com.avanteweb.a10.services;

import java.util.List;

import br.com.avanteweb.a10.entities.UsuarioCliente;
import br.com.avanteweb.a10.exception.services.ErroService;

public interface UsuarioClienteService {
	
	public UsuarioCliente persistir(UsuarioCliente usuarioCliente) throws ErroService;

	public void deleteUsuarioClienteById(Long usuarioCliente) throws ErroService;

	public UsuarioCliente findUsuarioClienteById(Long usuarioCliente) throws ErroService;
	
	public List<UsuarioCliente> getAllUsuarioCliente() throws ErroService;
		
	public UsuarioCliente findByIdUsuario(Long idUsuario) throws ErroService;
	
	public List<UsuarioCliente> findByIdCliente(Long idCliente) throws ErroService;
}
