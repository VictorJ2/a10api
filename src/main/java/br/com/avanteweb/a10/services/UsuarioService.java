package br.com.avanteweb.a10.services;

import java.util.List;
import java.util.Optional;

import br.com.avanteweb.a10.entities.Usuario;
import br.com.avanteweb.a10.exception.services.ErroService;

public interface UsuarioService {
	public Usuario persistir(Usuario usuario) throws ErroService;
	
	public void deletar(Usuario usuario) throws ErroService;
	
	public void deletarUsuarioById(Long id) throws ErroService;
	
	public Optional<Usuario> findUsuariobyId(Long IdUsuario) throws ErroService;
	
	public List<Usuario> getAllUsuario() throws ErroService;
	
	public Boolean findByEmail(String email) throws ErroService;
	
	public Usuario findUsuarioByIdGfsis(Long idGfsis) throws ErroService;
}
