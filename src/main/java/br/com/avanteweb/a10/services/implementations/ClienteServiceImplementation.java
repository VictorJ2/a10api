package br.com.avanteweb.a10.services.implementations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import br.com.avanteweb.a10.entities.Cliente;
import br.com.avanteweb.a10.exception.services.ErroService;
import br.com.avanteweb.a10.repositories.ClienteRepository;
import br.com.avanteweb.a10.services.ClienteService;

@Service
@EnableJpaRepositories("br.com.avanteweb.a10.repositories")
@EntityScan("br.com.avanteweb.a10.entities")
public class ClienteServiceImplementation implements ClienteService{
	
	private static final Logger log = LoggerFactory.getLogger(ClienteServiceImplementation.class);
	
	@Autowired
	ClienteRepository cliRepository;

	@Override
	public Cliente persistir(Cliente cliente) throws ErroService {
		
		try {
			cliente.setMunicipio(cliRepository.findNumeroIBGE(cliente.getMunicipio(), cliente.getUF()));
			Cliente newCliente = this.cliRepository.save(cliente);
			log.info("Cliente cadastrado com sucesso! Dados: "+cliente.toString());
			return newCliente;
		} catch (Exception e) {
			log.warn("Erro ao cadastrar cliente. Dados do cliente: "+cliente.toString());
			return null;
		}
	}

	@Override
	public void deletar(Cliente cliente) throws ErroService{
		try {
			cliRepository.delete(cliente);
			log.info("Cliente deletado: " +cliente.toString());
		} catch (Exception e) {
			log.warn("Erro ao deletar cliente. Dados do cliente: "+cliente.toString());
			throw new ErroService("Erro ao deletar Cliente de id: " + cliente.getIdCliente(), e);
		}
	}
	
	@Override
	public Cliente findClientebyId(Long IdCliente) throws ErroService{
		try {
			Optional<Cliente> optionCliente = cliRepository.findById(IdCliente);
			if(optionCliente.isPresent()) {
				log.info("Cliente encontrado com sucesso! Dados: "+optionCliente.get());
				return optionCliente.get();
			}
			log.info("Usuario não encontrado!");
			return null;
		} catch (Exception e) {
			log.warn("Erro ao executar busca por IdCliente: "+IdCliente+" !");
			return null;		}
	}
	
	@Override
	public Cliente findClienteByIdGfsis(Long id) throws ErroService{
		try {
			Cliente cliente = cliRepository.findByIdGfsis(id);
			if(cliente != null) {
				log.info("Cliente encontrado com sucesso! Dados: "+cliente.toString());
				return cliente;
			}else {
				log.info("Cliente não encontrado!");
				return null;
			}
		} catch (Exception e) {
			log.warn("Erro ao executar busca por IdGFSIS: "+id+" !");
			return null;		}
	}

	@Override
	public List<Cliente> getAllClientes() throws ErroService {
		try {
			List<Cliente> listaClientes = new ArrayList<Cliente>();
			for(Cliente cliente: cliRepository.findAll()) {
				listaClientes.add(cliente);
			}
			return listaClientes;
		} catch (Exception e) {
			log.warn("Erro ao retornar todos Clientes!");
			return null;		}
	}
}
