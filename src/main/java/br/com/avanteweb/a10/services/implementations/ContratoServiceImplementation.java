package br.com.avanteweb.a10.services.implementations;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.avanteweb.a10.entities.Contrato;
import br.com.avanteweb.a10.exception.services.ErroService;
import br.com.avanteweb.a10.repositories.ContratoRepository;
import br.com.avanteweb.a10.services.ContratoService;

@Service
public class ContratoServiceImplementation implements ContratoService{
	private static final Logger log = LoggerFactory.getLogger(ContratoServiceImplementation.class);
	
	@Autowired
	private ContratoRepository contratoRepository; 

	@Override
	public Contrato persistir(Contrato contrato) throws ErroService{
		try {
			log.info("Contrato cadastrado com sucesso! Dados: "+contrato.toString());
			return contratoRepository.save(contrato);
		} catch (Exception e) {
			log.warn("Erro ao cadastrar Contrato! Dados: "+contrato.toString());
			return null;		}
	}

	@Override
	public void deleteContratoById(Long idContrato) throws ErroService{
		try {
			contratoRepository.deleteById(idContrato);
			log.info("Contrato deletado com sucesso!");
		} catch (Exception e) {
			log.warn("Erro ao deletar contrato! Id: " + idContrato);
			throw new ErroService("Erro ao deletar o Contrato de id: " + idContrato, e);
		}
	}

	@Override
	public Contrato findContratoById(Long idContrato) throws ErroService{
		try {
			log.info("Busca pelo contrato efetuada com sucesso!");
			return contratoRepository.findById(idContrato).orElse(null);
		} catch (Exception e) {
			log.warn("Erro ao busca contrato! Id: "+idContrato);
			return null;		}
	}

	@Override
	public Contrato findContratoByIdGfsis(Long idContrato) throws ErroService{
		try {
			Contrato opContrato = contratoRepository.findByIdGfsis(idContrato);
			log.info("Contrato encontrado com sucesso! Dados: "+opContrato.toString());
			return opContrato;
		} catch (Exception e) {
			log.warn("Erro ao busca contrato! IdGFSIS: "+idContrato);
			return null;		}
	}

	@Override
	public List<Contrato> getAllContratos() throws ErroService{
		try {
			List<Contrato> listContrato = new ArrayList<Contrato>();
			for(Contrato contrato: contratoRepository.findAll()) {
				listContrato.add(contrato);
			}
			log.info("Todos contratos buscados com sucesso!");
			return listContrato;
		} catch (Exception e) {
			log.warn("Erro ao buscar todos contratos!");
			return null;		}
	}

	@Override
	public List<Contrato> findContratoByIdCliente(Long idCliente) throws ErroService {
		try {
//			Contrato contrato = findContratoByIdCliente(idCliente);
			List<Contrato> contrato = contratoRepository.findByIdCliente(idCliente);
			log.info("Contrato pelo id do cliente encontrado com sucesso! Dados: "+contrato.toString());
			return contrato;
		} catch (Exception e) {
			log.warn("Erro ao busca contrato pelo id do cliente! Id: "+idCliente);
			return null;		}
	}
}
