package br.com.avanteweb.a10.services.implementations;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import br.com.avanteweb.a10.entities.UsuarioCliente;
import br.com.avanteweb.a10.exception.services.ErroService;
import br.com.avanteweb.a10.repositories.UsuarioClienteRepository;
import br.com.avanteweb.a10.services.UsuarioClienteService;

@Service
@EnableJpaRepositories("br.com.avanteweb.a10.repositories")
@EntityScan("br.com.avanteweb.a10.entities")
public class UsuarioClienteServiceImplementation implements UsuarioClienteService {
	private static final Logger log = LoggerFactory.getLogger(UsuarioClienteServiceImplementation.class);
	
	@Autowired
	UsuarioClienteRepository userCliRepository;

	@Override
	public UsuarioCliente persistir(UsuarioCliente usuarioCliente) throws ErroService{
		try {
			UsuarioCliente obj = userCliRepository.save(usuarioCliente);
			log.info("UsuarioCliente cadastro com sucesso! Dados: "+usuarioCliente.toString());
			return obj;
		} catch (Exception e) {
			log.warn("Erro ao cadastrar UsuarioCliente, dados: "+usuarioCliente.toString());
			return null;		} 
	}

	@Override
	public void deleteUsuarioClienteById(Long usuarioCliente) throws ErroService{
		try {
			userCliRepository.deleteById(usuarioCliente);
			log.info("UsuarioCliente deletado com sucesso!");
		} catch (Exception e) {
			log.warn("Erro ao deletar UsuarioCliente! ID: "+usuarioCliente);
			throw new ErroService("Erro ao deletar Usuario_Cliente!", e);
		}
	}
	
	@Override
	public UsuarioCliente findByIdUsuario(Long idUsuario) throws ErroService{
		try {
			log.info("UsuarioCliente encontrado com sucesso!");
			return userCliRepository.findByIdUsuario(idUsuario);
		} catch (Exception e) {
			log.warn("Erro ao buscar UsuarioCliente. ID: "+idUsuario);
			return null;		}
	}

	@Override
	public List<UsuarioCliente> getAllUsuarioCliente() throws ErroService{
		try {
			List<UsuarioCliente> listaUserCli = new ArrayList<UsuarioCliente>();
			for(UsuarioCliente userCli: userCliRepository.findAll()) {
				listaUserCli.add(userCli);
			}
			log.info("Lista UsuarioCliente encontrado com sucesso!");
			return listaUserCli;
		} catch (Exception e) {
			log.warn("Erro ao buscar lista UsuarioCliente");
			return null;		}
	}

	@Override
	public UsuarioCliente findUsuarioClienteById(Long usuarioCliente) throws ErroService {
		try {
			UsuarioCliente obj = userCliRepository.findByIdCliente(usuarioCliente);
			log.info("UsuarioCliente encontrado com sucesso!");
			return obj;
		} catch (Exception e) {
			log.warn("Erro so buscar UsuarioCliente! Dado UsuarioClientee: "+usuarioCliente);
			return null;		}
	}

	@Override
	public List<UsuarioCliente> findByIdCliente(Long idCliente) throws ErroService {
		try {
			List<UsuarioCliente> listaUC = new ArrayList<UsuarioCliente>();
			for(UsuarioCliente x: userCliRepository.findAll()) {
				if(x.getIdCliente().equals(idCliente)) {
					listaUC.add(x);
				}
			}
			log.info("UsuarioCliente encontrado com sucesso!");
			return listaUC;
		} catch (Exception e) {
			log.warn("Erro ao buscar UsuarioCliente por id Cliente: " + idCliente);
			return null;		}
	}
}
