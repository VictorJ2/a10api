package br.com.avanteweb.a10.services.implementations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import br.com.avanteweb.a10.entities.Usuario;
import br.com.avanteweb.a10.exception.services.ErroService;
import br.com.avanteweb.a10.repositories.UsuarioRepository;
import br.com.avanteweb.a10.services.UsuarioService;

@Service
@EnableJpaRepositories("br.com.avanteweb.a10.repositories")
@EntityScan("br.com.avanteweb.a10.entities")
public class UsuarioServiceImplementation implements UsuarioService{
	private static final Logger log = LoggerFactory.getLogger(UsuarioServiceImplementation.class);
	
	@Autowired
	UsuarioRepository userRepository;

	@Override
	public Usuario persistir(Usuario usuario) throws ErroService{
		try {
			log.info("Tentativa de cadastrar usuário! Dados: "+usuario.toString());
			return userRepository.save(usuario);
		} catch (Exception e) {
			log.warn("Erro ao cadastrar usuario. Dados: "+usuario.toString());
			return null;		}
	}

	@Override
	public void deletar(Usuario usuario) throws ErroService{
		try {
			log.info("Usuario deltado com sucesso!");
			userRepository.delete(usuario);
		} catch (Exception e) {
			log.warn("Erro ao deletar usuario. Dados usuario: "+usuario.toString());
			throw new ErroService("Erro ao deletar Usuário!", e);
		}
	}
	
	@Override
	public void deletarUsuarioById(Long id) throws ErroService{
		try {
			log.info("Usuario deletado com sucesso!");
			userRepository.deleteById(id);
		} catch (Exception e) {
			log.warn("Erro ao deletar usuario. Id: "+id);
			throw new ErroService("Erro ao deletar Usuário por ID!", e);
		}
	}

	@Override
	public Optional<Usuario> findUsuariobyId(Long idUsuario) throws ErroService{
		try {
			log.info("Usuário encontrado com sucesso!");
			return userRepository.findById(idUsuario);
		} catch (Exception e) {
			log.warn("Erro ao buscar usaurio. ID: "+idUsuario);
			return null;		}
	}

	@Override
	public List<Usuario> getAllUsuario() throws ErroService{
		try {
			List<Usuario> listaUsuarios = new ArrayList<Usuario>();
			for(Usuario usuario: userRepository.findAll()) {
				listaUsuarios.add(usuario);
			}
			log.info("Lista de usuario obtida com sucesso!");
			return listaUsuarios;
		} catch (Exception e) {
			log.warn("Erro ao buscar lista de usuarios!");
			return null;		}
	}

	@Override
	public Usuario findUsuarioByIdGfsis(Long idGfsis) throws ErroService {
		try {
			Usuario usuario = userRepository.findByIdGfsis(idGfsis);
			log.info("Encontrado usuario pelo IDGFSIS com sucesso");
			return usuario;
		} catch (Exception e) {
			log.warn("Erro ao encontrar usuario pelo idGFSIS. ID: "+idGfsis);
			return null;		}
	}

	@Override
	public Boolean findByEmail(String email) throws ErroService {
		try {
			Usuario usuario = userRepository.findByEmail(email);
			//Verifica se usuario com este email existe
			if(usuario != null) {
				//Se ele existir retorna true
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			log.warn("Erro ao buscar email do Usuário no banco: " +e.getCause());
			return null;
		}
	}
}
