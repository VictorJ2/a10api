package br.com.avanteweb.a10.utils;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PasswordUtil {
    
    private static final Logger log = LoggerFactory.getLogger(PasswordUtil.class);

    public PasswordUtil() {

    }

    /**
     * Gera um hash utilizando BCrypt
     * 
     * @param senha
     * @return String
     */
    public static String gerarBCrypt(Long codigo, String senha) {
        if (senha == null) {
            return senha;
        }

        log.info("Gerando hash com o BCrypt.");
        // BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();
        // return bCryptEncoder.encode(senha);
        return HashMaker.stringHexa(HashMaker.gerarHash(codigo + senha));
    }

    /**
     * Gera um hash utilizando BCrypt
     * 
     * @param senha
     * @return String
     */
    public static String gerarBCrypt(BigDecimal codigo, String senha) {
        if (senha == null) {
            return senha;
        }

        log.info("Gerando hash com o BCrypt.");
        // BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();
        // return bCryptEncoder.encode(senha);
        return HashMaker.stringHexa(HashMaker.gerarHash(codigo + senha));
    }
}