package br.com.avanteweb.a10.utils;

import java.text.Normalizer;

public class RemoveAcentos {
	public static String removeAcentos(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str; 
    }
}
